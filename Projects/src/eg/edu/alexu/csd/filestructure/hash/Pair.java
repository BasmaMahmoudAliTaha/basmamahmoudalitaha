package eg.edu.alexu.csd.filestructure.hash;

public class Pair<K, V> {
	
	private K key;
	private V value;
	
	//===============Constructor================//
	
	public void setValue(V value) {
		this.value = value;
	}

	public Pair (K key, V value) {
		
		this.key = key;
		this.value = value;
		
	}
	
	public K getKey() {
		return key;
	}
	
	public V getValue() {
		return value;
	}
}
