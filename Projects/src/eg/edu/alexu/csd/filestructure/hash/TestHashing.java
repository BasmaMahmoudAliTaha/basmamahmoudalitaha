package eg.edu.alexu.csd.filestructure.hash;

import static org.junit.Assert.*;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

public class TestHashing {

	/// Test Before needing to Rehash
	@Test
	public void testChain1() {
		IHash<Integer, String> chain = new ChainingHash<Integer, String>();

		assertEquals(1200, chain.capacity());
		chain.put(1, "a");
		assertEquals(1, chain.size());
		chain.put(2, "n");
		assertEquals(2, chain.size());

		chain.put(3, "b");
		assertEquals(3, chain.size());
		chain.put(4, "c");
		assertEquals(4, chain.size());
		assertEquals("a", chain.get(1));

		//////////////////// Contains ///////////////////////
		assertEquals(null, chain.get(5));
		assertEquals(true, chain.contains(4));
		assertEquals(false, chain.contains(7));
		assertEquals(true, chain.contains(1));
		assertEquals(false, chain.contains(10));

		/////////////////// Delete //////////////////////////
		chain.delete(5);
		assertEquals(4, chain.size());

		chain.delete(2);
		assertEquals(3, chain.size());

		chain.delete(1);
		assertEquals(2, chain.size());

		chain.delete(2);
		assertEquals(2, chain.size());

		chain.delete(3);
		assertEquals(1, chain.size());

		assertEquals(false, chain.isEmpty());

		chain.delete(4);
		assertEquals(0, chain.size());

		assertEquals(true, chain.isEmpty());
	}
	@Test
	public void testLinear1() {
		IHash<Integer, String> linear = new LinearProbing<Integer, String>();

		assertEquals(1200, linear.capacity());
		linear.put(1, "a");
		assertEquals(1, linear.size());
		linear.put(2, "n");
		assertEquals(2, linear.size());

		linear.put(3, "b");
		assertEquals(3, linear.size());
		linear.put(4, "c");
		assertEquals(4, linear.size());
		assertEquals(1200, linear.capacity());
		assertEquals("a", linear.get(1));

		////////////////////Contains ///////////////////////
		assertEquals(null, linear.get(5));
		assertEquals(true, linear.contains(4));
		assertEquals(false, linear.contains(7));
		assertEquals(true, linear.contains(1));
		assertEquals(false, linear.contains(10));

		/////////////////// Delete //////////////////////////
		linear.delete(5);
		assertEquals(4, linear.size());

		linear.delete(2);
		assertEquals(3, linear.size());

		linear.delete(1);
		assertEquals(2, linear.size());

		linear.delete(2);
		assertEquals(2, linear.size());

		linear.delete(3);
		assertEquals(1, linear.size());

		assertEquals(false, linear.isEmpty());

		linear.delete(4);
		assertEquals(0, linear.size());

		assertEquals(true, linear.isEmpty());
	}
	@Test
	public void testQuadratic1() {
		IHash<Integer, String> quadratic = new QuadraticProbing<Integer, String>();
		assertEquals(1200, quadratic.capacity());
		quadratic.put(1, "a");
		assertEquals(1, quadratic.size());
		quadratic.put(2, "n");
		assertEquals(2, quadratic.size());

		quadratic.put(3, "b");
		assertEquals(3, quadratic.size());
		quadratic.put(4, "c");
		assertEquals(4, quadratic.size());
		assertEquals(1200, quadratic.capacity());
		assertEquals("a", quadratic.get(1));


		////////////////////Contains ///////////////////////
		assertEquals(null, quadratic.get(5));
		assertEquals(true, quadratic.contains(4));
		assertEquals(false, quadratic.contains(7));
		assertEquals(true, quadratic.contains(1));
		assertEquals(false, quadratic.contains(10));

		/////////////////// Delete //////////////////////////
		quadratic.delete(5);
		assertEquals(4, quadratic.size());

		quadratic.delete(2);
		assertEquals(3, quadratic.size());

		quadratic.delete(1);
		assertEquals(2, quadratic.size());

		quadratic.delete(2);
		assertEquals(2, quadratic.size());

		quadratic.delete(3);
		assertEquals(1, quadratic.size());

		assertEquals(false, quadratic.isEmpty());

		quadratic.delete(4);
		assertEquals(0, quadratic.size());

		assertEquals(true, quadratic.isEmpty());
	}

	/// Test after needing to Rehash

	@Test
	public void testChain2() {
		IHash<Integer, String> chain = new ChainingHash<Integer, String>();

		assertEquals(1200, chain.capacity());
		chain.put(1, "aaa");
		assertEquals(1, chain.size());
		chain.put(2, "n");
		assertEquals(2, chain.size());

		chain.put(3, "basma");
		assertEquals(3, chain.size()); //test duplicate
		chain.put(3, "why");

		assertEquals("why", chain.get(3));

		assertEquals(3, chain.size());
		chain.put(2, "ma");
		assertEquals("ma", chain.get(2));
		assertEquals(3, chain.size());
		Set<Integer> keys = new HashSet<>();
		keys.add(1);
		keys.add(2);
		keys.add(3);
		assertEquals(keys, chain.keys());


		//////////////////// Contains ///////////////////////
		assertEquals(null, chain.get(5));
		assertEquals(false, chain.contains(4));
		assertEquals(false, chain.contains(7));
		assertEquals(true, chain.contains(1));
		assertEquals(false, chain.contains(10));

		/////////////////// Delete //////////////////////////
		chain.delete(5);
		assertEquals(3, chain.size());

		chain.delete(2);
		assertEquals(2, chain.size());

		chain.delete(1);
		assertEquals(1, chain.size());
		assertEquals(false, chain.isEmpty());

		chain.delete(3);
		assertEquals(0, chain.size());
		assertEquals(true, chain.isEmpty());

		chain.put(10, "hkhd");

		assertEquals(false, chain.isEmpty());
		assertEquals(1, chain.size());
		assertEquals("hkhd", chain.get(10));
	}
	@Test
	public void testLinear2() {
		IHash<Integer, String> linear = new LinearProbing<Integer, String>();

		assertEquals(1200, linear.capacity());
		linear.put(1, "a");
		assertEquals(1, linear.size());
		linear.put(2, "n");
		assertEquals(2, linear.size());

		linear.put(3, "b");
		assertEquals(3, linear.size());
		linear.put(4, "c");
		assertEquals(4, linear.size());
		assertEquals(1200, linear.capacity());
		assertEquals("a", linear.get(1));

		////////////////////Contains ///////////////////////
		assertEquals(null, linear.get(5));
		assertEquals(true, linear.contains(4));
		assertEquals(false, linear.contains(7));
		assertEquals(true, linear.contains(1));
		assertEquals(false, linear.contains(10));

		/////////////////// Delete //////////////////////////
		linear.delete(5);
		assertEquals(4, linear.size());

		linear.delete(2);
		assertEquals(3, linear.size());

		linear.delete(1);
		assertEquals(2, linear.size());

		linear.delete(2);
		assertEquals(2, linear.size());

		linear.delete(3);
		assertEquals(1, linear.size());

		assertEquals(false, linear.isEmpty());

		linear.delete(4);
		assertEquals(0, linear.size());

		assertEquals(true, linear.isEmpty());
	}
	@Test
	public void testQuadratic2() {
		IHash<Integer, String> quadratic = new QuadraticProbing<Integer, String>();
		assertEquals(1200, quadratic.capacity());
		quadratic.put(1, "a");
		assertEquals(1, quadratic.size());
		quadratic.put(2, "n");
		assertEquals(2, quadratic.size());

		quadratic.put(3, "b");
		assertEquals(3, quadratic.size());
		quadratic.put(4, "c");
		assertEquals(4, quadratic.size());
		assertEquals(1200, quadratic.capacity());
		assertEquals("a", quadratic.get(1));


		////////////////////Contains ///////////////////////
		assertEquals(null, quadratic.get(5));
		assertEquals(true, quadratic.contains(4));
		assertEquals(false, quadratic.contains(7));
		assertEquals(true, quadratic.contains(1));
		assertEquals(false, quadratic.contains(10));

		/////////////////// Delete //////////////////////////
		quadratic.delete(5);
		assertEquals(4, quadratic.size());

		quadratic.delete(2);
		assertEquals(3, quadratic.size());

		quadratic.delete(1);
		assertEquals(2, quadratic.size());

		quadratic.delete(2);
		assertEquals(2, quadratic.size());

		quadratic.delete(3);
		assertEquals(1, quadratic.size());

		assertEquals(false, quadratic.isEmpty());

		quadratic.delete(4);
		assertEquals(0, quadratic.size());

		assertEquals(true, quadratic.isEmpty());
	}
	
	@Test
	public void testDouble1() {
		IHash<Integer, String> doubleP = new DoubleProbing<Integer, String>();

		assertEquals(1200, doubleP.capacity());
		doubleP.put(1, "a");
		assertEquals(1, doubleP.size());
		doubleP.put(2, "n");
		assertEquals(2, doubleP.size());

		doubleP.put(3, "b");
		assertEquals(3, doubleP.size());
		doubleP.put(4, "c");
		assertEquals(4, doubleP.size());
		assertEquals("a", doubleP.get(1));

		//////////////////// Contains ///////////////////////
		assertEquals(null, doubleP.get(5));
		assertEquals(true, doubleP.contains(4));
		assertEquals(false, doubleP.contains(7));
		assertEquals(true, doubleP.contains(1));
		assertEquals(false, doubleP.contains(10));

		/////////////////// Delete //////////////////////////
		doubleP.delete(5);
		assertEquals(4, doubleP.size());

		doubleP.delete(2);
		assertEquals(3, doubleP.size());

		doubleP.delete(1);
		assertEquals(2, doubleP.size());

		doubleP.delete(2);
		assertEquals(2, doubleP.size());

		doubleP.delete(3);
		assertEquals(1, doubleP.size());

		assertEquals(false, doubleP.isEmpty());

		doubleP.delete(4);
		assertEquals(0, doubleP.size());

		assertEquals(true, doubleP.isEmpty());
	}
}
