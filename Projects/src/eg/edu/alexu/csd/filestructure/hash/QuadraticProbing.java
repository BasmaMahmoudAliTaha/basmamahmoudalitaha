package eg.edu.alexu.csd.filestructure.hash;

import java.util.HashMap;
import java.util.Set;

public class QuadraticProbing<K, V> implements IHashQuadraticProbing, IHash<K, V> {

	//=================== Attributes =====================//

	private Pair<K,V>[] hashTable;
	private int collisions, capacity, size;
	private HashMap<K, Integer> keys;

	//=================== Constructor =====================//

	@SuppressWarnings("unchecked")
	public QuadraticProbing(){
		collisions = 0;
		keys = new HashMap<K, Integer>();
		capacity = 1200;
		size =  0;
		hashTable = new Pair[capacity];

	}
	@SuppressWarnings("unchecked")
	@Override
	
	public void put(K key, V value) {

		if(key == null) {
			throw new RuntimeException("Null Key!");
		}

		if(keys.containsKey(key)) { // For duplicate keys, just update the key's value
			updateKey(key, value);
			return;
		}
		boolean inserted = map(key, value);
		if(!inserted) { // Maximum number of probing reached.
			Pair<K,V>[] hashCopy = new Pair[capacity];
			++collisions;
			System.arraycopy(hashTable, 0, hashCopy, 0, hashTable.length);
			capacity *= 2; // update the capacity
			hashTable = new Pair[capacity];
			rehash(hashCopy);
			map(key, value);
		}
		size++;
		keys.put(key, 0);
	}
	@Override
	public String get(K key) {

		if (key == null) { 
			throw new RuntimeException("Null arguement!");
		}
		
		if(!keys.containsKey(key) || size == 0) { // key isn't in the table
			return null;
		}

		int keyPlace = search(key);
		if(keyPlace != -1) { //  key is found

			return (String) hashTable[keyPlace].getValue();
		}
		return null;
	}

	@Override
	public void delete(K key) {

		if (key == null) { 
			throw new RuntimeException("Null arguement!");
		}
		if(!keys.containsKey(key) || size == 0) { // key isn't in the table
			return;
		}
		int keyPlace = search(key);
		if(keyPlace != -1) { //  key is found, if not found do nothing

			hashTable[keyPlace] = null;
			size--;
			keys.remove(key);

		}
	}

	@Override
	public boolean contains(K key) {
		if (key == null) { 
			throw new RuntimeException("Null arguement!");
		}
		if(!keys.containsKey(key) || size == 0) { // key isn't in the table
			return false;
		}
		return search(key) != -1 ? true : false;
	}

	@Override
	public boolean isEmpty() {

		return size == 0 ? true : false;
	}

	@Override
	public int size() {

		return size;
	}

	@Override
	public int capacity() {

		return capacity;
	}

	@Override
	public int collisions() {

		return collisions;
	}

	@Override
	public Iterable<K> keys() {
		Set<K> keySet = keys.keySet();
		return keySet;
	}

	//=============== Locally used Functions ===================//

	
	private boolean map(K key, V value) {
		int i = 0,h, h1;
		h =  key.hashCode() % capacity;
		h1 = (h  + i * i) % capacity;
		int temp = h1;
		
		while (i < capacity) {

			if(hashTable[h1] == null) {
				hashTable[h1] = new Pair<K,V>(key,value);
				if(h1 != temp){
					++collisions;
				}
				return true; // inserted
			}
			else {
				++i;
				h1 = (h  + i * i) % capacity;
			}
			++collisions;
		}
		return false;
	}

	private void rehash(Pair<K, V>[] hashCopy) {

		for (int i = 0 ; i < hashCopy.length; ++i) {
			if(hashCopy[i] == null){
				continue;
			}
			map(hashCopy[i].getKey(), hashCopy[i].getValue());
		}
	}

	private void updateKey(K key, V value) {

		int keyPlace = search(key);
		hashTable[keyPlace].setValue(value);
	}
	
	private int search(K key) {
		int i = 0,h, h1;
		h =  key.hashCode() % capacity;
		h1 = (h  + i * i) % capacity;
		
		while (i != capacity) {

			if(hashTable[h1] != null && hashTable[h1].getKey().equals(key)) {
				return h1; // Found, return index
			}
			else {
				++i;
				h1 = (h  + i * i) % capacity;
			}
		}
		return -1; // there is no enough rooms
	}
}
