package eg.edu.alexu.csd.filestructure.hash;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Set;

public class ChainingHash <K,V> implements IHashChaining, IHash<K, V> {

	//=================== Attributes =====================//

	private LinkedList<Pair<K, V>>[] hashTable;
	private int collisions, size, capacity;
	private HashMap<K, Integer> keys;

	//=================== Constructor =====================//

	@SuppressWarnings("unchecked")
	public ChainingHash(){

		collisions = 0;
		keys = new HashMap<K, Integer>();
		capacity = 1200;
		size =  0;
		hashTable = new LinkedList[capacity];

	}
	@Override
	public void put(K key, V value) {

		if(key == null) {
			throw new RuntimeException("Null arguement!");
		}
		if(keys.containsKey(key)) { // Duplicate key.
			updateKey(key, value);
			return;
		}
		map(key, value);
		size++;
		keys.put(key, 0);
	}

	@Override
	public String get(K key) {
		if(key == null) {
			throw new RuntimeException("Null arguement!");
		}
		if(!keys.containsKey(key) || size == 0) { // key isn't in the table
			return null;
		}
		LinkedList<Pair<K, V>> chainedKeys = hashTable[(key.hashCode()) % hashTable.length];
		for(int i = 0; i < chainedKeys.size(); i++){
			if(chainedKeys.get(i).getKey().equals(key)){
				return (String) chainedKeys.get(i).getValue();
			}
		}
		return null; // Key doesn't exist in hashTable.
	}

	@Override
	public void delete(K key) {
		if(key == null) {
			throw new RuntimeException("Null key!");
		}
		if(!keys.containsKey(key) || size == 0) { // key isn't in the table
			return ;
		}
		LinkedList<Pair<K, V>> chain = hashTable[(key.hashCode()) % hashTable.length];
		if(!chain.isEmpty()) {
			chain.remove(key);
			size--;
			keys.remove(key);
		}
	}

	@Override
	public boolean contains(K key) {

		if(key == null) {
			throw new RuntimeException("Null arguement!");
		}

		return !keys.containsKey(key)? false : true;
	}

	@Override
	public boolean isEmpty() {

		return size == 0 ? true : false;
	}

	public int size() {

		return size;
	}

	@Override
	public int capacity() {

		return capacity;
	}

	@Override
	public int collisions() {

		return collisions;
	}

	@Override
	public Iterable<K> keys() {
		Set<K> setKeys = keys.keySet();
		return setKeys;
	}

	//=============== Locally used Functions ===================//

	private void map(K key, V value) {

		int i = (key.hashCode()) % hashTable.length;

		if(hashTable[i] != null) { // this means that place 'i' is occupied in hashtable already
			collisions += hashTable[i].size();
			hashTable[i].add(new Pair<K,V>(key, value));
			return;
		}

		LinkedList<Pair<K,V>> list = new LinkedList<Pair<K,V>>();
		list.add(new Pair<K,V>(key, value));
		hashTable[i] = list;
	}
	
	private void updateKey(K key, V value) {
		LinkedList<Pair<K,V>> chainingList = hashTable[(key.hashCode()) % hashTable.length];
		for(int j = 0; j < chainingList.size(); ++j) {
			collisions++;
			Pair<K,V> elem = chainingList.get(j);
			if (elem.getKey().equals(key)) {
				elem.setValue(value);
				break;
			}
		}
	}
}
