package eg.edu.alexu.csd.filestructure.avl;

public class AvlTree<T extends Comparable<T>> implements IAVLTree<T> {

	private Node<T> root;
	private final int ALLOWED_IMBALANCE = 1;
	private boolean isDictionary, deleted = false;
	private int size = 0; // indicator for number of nodes in the tree


	public AvlTree(boolean isDictionary) {
		this.isDictionary = isDictionary;
	}
	public  AvlTree() {}

	/*
	 * Assumption:
	 * if the element is duplicate it will go to the left,
	 * in case it's not a dictionary
	 * 
	 */
	private Node<T> insert( T key, Node<T> rootOfSubTree)
	{
		/*Base case*/
		if( rootOfSubTree == null ){//insert the key

			size++;
			return new Node<T>( key, null, null );

		}



		int compareResult = key.compareTo( rootOfSubTree.getValue() );

		if( compareResult < 0 || ( compareResult == 0 && !isDictionary ) ) {

			Node<T> x = rootOfSubTree.getLeftChild();
			Node<T> y = insert( key, x ) ;
			rootOfSubTree.setLeftChild(y) ;

		}


		else if( compareResult > 0 ){

			Node<T> x = rootOfSubTree.getRightChild() ;
			Node<T> y = insert( key, x ) ;
			rootOfSubTree.setRightChild( y );

		}


		return balance( rootOfSubTree );
	}
	@Override
	public void insert(T key) {

		if(key == null){
			return;
		}
		if(root == null){
			root = new Node<T>( key, null, null );
			size++ ;
		}

		else
			root = insert( key, root );

	}

	private Node<T> balance(Node<T> root){

		if(root == null)
			return root;
		Node<T> l = root.getLeftChild() ;
		Node<T> r = root.getRightChild() ;
		Node<T> ll = null, lr = null ;
		if( l != null ){
			ll = l.getLeftChild() ;
			lr = l.getRightChild() ;
		}
		Node<T> rl = null, rr = null ;
		if( r != null ){
			rl = r.getLeftChild() ;
			rr = r.getRightChild() ;
		}


		if(getNodeHeight( l ) - getNodeHeight( r ) > ALLOWED_IMBALANCE){

			if( getNodeHeight( ll ) >= getNodeHeight( lr ) ){

				root = rotateWithLeftChild( root );
			}

			else{

				root = doubleWithLeftChild( root );
			}

		}
		else 
			if( getNodeHeight( r ) - getNodeHeight( l ) > ALLOWED_IMBALANCE ){

				if(getNodeHeight( rr ) >= getNodeHeight( rl ) ){

					root = rotateWithRightChild( root );
				}

				else{

					root = doubleWithRightChild( root );
				}
			}
		l = root.getLeftChild() ;
		r = root.getRightChild() ;
		root.setHeight(Math.max( getNodeHeight( l ), getNodeHeight( r ) ) + 1);
		return root;
	}
	/**
	 * Rotate binary tree node with left child.
	 * For AVL trees, this is a single rotation for case 1.
	 * Update heights, then return new root.
	 */
	private Node<T> rotateWithLeftChild(Node<T> k2){

		Node<T> k1 = k2.getLeftChild();
		k2.setLeftChild( k1.getRightChild() );
		k1.setRightChild( k2 );
		k2.setHeight(Math.max( getNodeHeight( k2.getLeftChild() ), getNodeHeight( k2.getRightChild() ) ) + 1);
		k1.setHeight(Math.max(getNodeHeight( k1.getLeftChild() ), getNodeHeight(k2) ) + 1);

		return k1;
	}
	/**
	 * Double rotate binary tree node: first left child
	 * with its right child; then node k3 with new left child.
	 * For AVL trees, this is a double rotation for case 2.
	 * Update heights, then return new root.
	 */

	private Node<T> doubleWithLeftChild(Node<T> t){
		Node<T> z = t.getLeftChild() ;
		Node<T> y = rotateWithRightChild( z ) ;
		t.setLeftChild( y );
		return rotateWithLeftChild( t );
	}

	private Node<T> rotateWithRightChild(Node<T> k2){

		Node<T> k1 = k2.getRightChild();
		k2.setRightChild( k1.getLeftChild() );
		k1.setLeftChild( k2 );
		k2.setHeight(Math.max( getNodeHeight( k2.getRightChild() ), getNodeHeight( k2.getLeftChild() ) ) + 1);
		k1.setHeight(Math.max(getNodeHeight( k1.getRightChild() ), getNodeHeight(k2) ) + 1);

		return k1;

	}

	private Node<T> doubleWithRightChild(Node<T> t){

		Node<T> z = t.getRightChild() ;
		Node<T> y = rotateWithLeftChild( z ) ;
		t.setRightChild( y );
		return rotateWithRightChild( t );

	}

	private Node<T> findMin(Node<T> node){
		Node<T> x = node;
		Node<T> y  = x;
		while(x != null){
			y = x;
			x= x.getLeftChild();
		}
		return y;	
	}
	/*
	 * Assumption:
	 * Deleting the 1st occurrence of the node,
	 * if it is duplicated.
	 * @return null when item isn't found
	 */
	private Node<T> delete( T key, Node<T> rootOfSubtree ){

		if( rootOfSubtree == null ){
			// item not found
			deleted = false;
			return rootOfSubtree;
		}
			

		int compResult = key.compareTo(rootOfSubtree.getValue());

		if( compResult < 0 ){
			Node<T> x = rootOfSubtree.getLeftChild();
			Node<T> y = delete( key, x );
			rootOfSubtree.setLeftChild( y );
		}

		else if( compResult > 0 ){
			Node<T> x = rootOfSubtree.getRightChild();
			Node<T> y = delete( key, x );
			rootOfSubtree.setRightChild( y );
		}

		/**Element is found*/

		// Case_1: Two children
		else if( rootOfSubtree.getLeftChild()!= null && rootOfSubtree.getRightChild() != null ){ 
			
			Node<T> x = rootOfSubtree.getRightChild() ;
			Node<T> y = findMin(x) ;
			rootOfSubtree.setValue( y.getValue() ) ;
			Node<T> u = rootOfSubtree.getRightChild();
			Node<T> z = delete( rootOfSubtree.getValue(), u ) ; 
			rootOfSubtree.setRightChild(z); 
			deleted = true;

		}

		// Case_2: One Child or none.
		else{ 
			Node<T> l = rootOfSubtree.getLeftChild();
			Node<T> r = rootOfSubtree.getRightChild();


			rootOfSubtree = ( l != null ) ? l : r;
			deleted = true;

		}

		return balance(rootOfSubtree);
	}

	@Override
	public boolean delete(T key) {
		if(root == null) // tree is empty
			return false;

		Node<T> result = delete(key, root);
		
		if(deleted){
			--size ;
			root = result ; 
			deleted =  false ;
			return true;
		}

		return false;
	}

	@Override
	public boolean search(T key) {
		if( key == null || root == null ) // key is null or tree is empty
			return false;

		int compareResult = key.compareTo(root.getValue());

		Node<T> x = root;
		while(x != null && compareResult != 0){
			compareResult = key.compareTo(x.getValue());
			if(compareResult < 0)
				x = x.getLeftChild();
			else
				x = x.getRightChild();
		}

		if(compareResult != 0)
			return false;
		else
			return true;
	}

	@Override
	public int height() {
		if(root == null)
			return 0;
		return root.getHeight();
	}

	@Override
	public INode<T> getTree() {

		return root;
	}

	public int getSize() {
		return size;
	}


	private int getNodeHeight(Node<T> node){
		if(node == null)
			return 0;
		else
			return node.getHeight();
	}
}
