package eg.edu.alexu.csd.filestructure.avl;

import java.io.File;

public class Main {
	
	public static void main(String[] args){
		
		Dictionary dictionary = new Dictionary();
		File file  = new File("dic.txt");
		dictionary.load(file);
		inOrder(dictionary.getDictionaryAvl().getTree());
		System.out.println( "Size "+ dictionary.size() );
		System.out.println( "Search (b) "+ dictionary.exists("b") );
		System.out.println( "Search (f) "+ dictionary.exists("f") );
		System.out.println( "Search (i) "+ dictionary.exists("i") );
		System.out.println( "Search (m) "+ dictionary.exists("m") );
		System.out.println( "Delete (q) "+ dictionary.delete("q") );
		System.out.println( "Delete (a) "+ dictionary.delete("a") );
		System.out.println( "Size "+ dictionary.size() );
		System.out.println( "Delete (y) "+ dictionary.delete("y") );
		System.out.println( "Size "+ dictionary.size() );
		System.out.println( "Delete (qqq) "+ dictionary.delete("qqq") );
		System.out.println( "Size "+ dictionary.size() );
		System.out.println( "Delete (a) "+ dictionary.delete("a") );
		System.out.println( "Size "+ dictionary.size() );
		System.out.println( "Delete (b) "+ dictionary.delete("b") );
		System.out.println( "Delete (c) "+ dictionary.delete("c") );
		System.out.println( "Delete (d) "+ dictionary.delete("d") );
		System.out.println( "Delete (e) "+ dictionary.delete("e") );
		System.out.println( "Delete (f) "+ dictionary.delete("f") );
		System.out.println( "Delete (g) "+ dictionary.delete("g") );
		System.out.println( "Size "+ dictionary.size() );
		inOrder(dictionary.getDictionaryAvl().getTree());
	}
	
	private static void inOrder(INode<String> x){
		if(x != null){
			inOrder(x.getLeftChild());
			System.out.println(x.getValue());
			inOrder(x.getRightChild());
		}
	}
	
}
