package eg.edu.alexu.csd.filestructure.avl;

public class Node<T extends Comparable<T>> implements INode<T> {
	private T value;
	private Node<T> leftChild, rightChild;
	private int height;

	/**Constructors***/
	public Node(T elem) {
		this(elem, null, null);
	}


	public Node(T elem, Node<T> left, Node<T> right){
		value = elem ;
		leftChild = left ;
		rightChild = right ;
		height = 1 ;
	}
	/**********************************************************/
	
	@Override
	public Node<T> getLeftChild() {

		return leftChild;
	}

	@Override
	public Node<T> getRightChild() {

		return rightChild;
	}

	public void setLeftChild(Node<T> leftChild) {
		
		this.leftChild = leftChild;
		
	// this.setHeight(Math.max( getNodeHeight( this.rightChild ), getNodeHeight( this.leftChild ) ) + 1);
	}


	public void setRightChild(Node<T> rightChild) {
		
		this.rightChild = rightChild;
		
	//	this.setHeight(Math.max( getNodeHeight( this.rightChild), getNodeHeight(this.leftChild) ) + 1);
	}

	@Override
	public T getValue() {

		return value;
	}

	@Override
	public void setValue(T value) {

		this.value = value;
	}

	public int getHeight(){

		return height;
	}
	
	public void setHeight(int height) {
		
		this.height = height;
	}

}
