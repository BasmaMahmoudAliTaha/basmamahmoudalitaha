package eg.edu.alexu.csd.filestructure.graphs;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class BinHeap <T extends Comparable<T>> {
	private ArrayList<Node> heap;
	private Node root;
	private int size;

	public BinHeap() {
		heap = new ArrayList<Node>();
		root = BinHeap.this.new Node(0);
	}

	private class Node{

		private T value ;
		private int index;
		private int vertex;
		private Node(int index) {
			this.index = index;
		}

		public Node getLeftChild() {
			int index = 2 * this.index +1;
			if(index < BinHeap.this.size)
				return BinHeap.this.heap.get(index);
			else 
				return null;
		}

		public Node getRightChild() {
			int index = (2 * this.index +1) +1;
			if(index < BinHeap.this.size)
				return BinHeap.this.heap.get(index);
			else 
				return null;

		}
//
//		public Node getParent() {
//			int index = this.index/2;
//			if(index < BinHeap.this.size)
//				return BinHeap.this.heap.get(index);
//			else 
//				return null;
//
//		}
		public int getVertex(){
			return vertex;
		}
		public T getValue() {

			return value;
		}

		public void setValue(T value) {

			this.value = value;
		}

		public void setVertex(int vertex) {
			this.vertex = vertex;
		}

	}
	public Node getRoot() {

		if(!heap.isEmpty()){
			root = heap.get(0);
			return root;
		}
		else
			return null;
		
	}

	public int size() {
		return heap.size();
	}
	public int getVertexNum(int i){
		return (int) root.getVertex();
	}
	public void heapify(Node node) {

		try{

			boolean leLarge = false, rLarge = false;
			T left = null;
			if(node.getLeftChild()!=null){ // null means it doesn't have child
				left = node.getLeftChild().getValue();
			}
			T right = null;

			if(node.getRightChild()!=null){
				right = node.getRightChild().getValue();
			}
			T largest;

			if(left != null && left.compareTo(node.getValue()) < 0){
				largest = left;
				leLarge = true;
			}


			else
				largest = node.getValue();

			if(right != null  &&right.compareTo(largest) < 0){
				largest = right;
				rLarge = true;
			}


			if(largest != null &&largest.compareTo(node.getValue()) != 0){


				if(rLarge){
					node.getRightChild().setValue(node.getValue());
					int vertex = node.getRightChild().getVertex();
					node.getRightChild().setVertex(node.getVertex());
					rLarge = false;
					node.setValue(largest);
					node.setVertex(vertex);
					heapify(node.getRightChild());
				}
				else if(leLarge){
					node.getLeftChild().setValue(node.getValue());
					int vertex = node.getLeftChild().getVertex();
					node.getLeftChild().setVertex(node.getVertex());
					leLarge = false;
					node.setValue(largest);
					node.setVertex(vertex);
					heapify(node.getLeftChild());
				}

			}
		}catch(Exception e){
			return;
		}


	}

	public T extract() {
		T max = null;
		try {

			max = this.getRoot().getValue();
			heap.get(0).setValue(heap.get(size()-1).getValue());
			int vertex = heap.get(0).getVertex();
			heap.get(0).setVertex(heap.get(size()-1).getVertex());
			heap.get(size()-1).setValue(max);
			heap.get(size()-1).setVertex(vertex);
			heap.remove(heap.size()-1);

			size = heap.size();

			if(size != 0)
				heapify(heap.get(0));


		} catch (Exception e) {

			try {
				throw new Exception("extracting from empty heap");
			} catch (Exception e1) {}
		}
		return max;

	}

	public void insert(T element, int vertex) {

		if(element == null)
			return;

		if(heap.size() == 0){
			root = BinHeap.this.new Node(0);
			root.setValue(element);
			root.setVertex(vertex);
			heap.add(root);
			return;
		}
		Node node = BinHeap.this.new Node(this.size());
		node.setValue(element);
		node.setVertex(vertex);
		heap.add(node);
		int i = heap.size()-1;
		while(i > 0 && (heap.get(i/2).getValue().compareTo(heap.get(i).getValue()) > 0)){

			T temp = heap.get(i/2).getValue();
			heap.get(i/2).setValue(heap.get(i).getValue());
			int vertexTmp = heap.get(i/2).getVertex();
			heap.get(i/2).setVertex(heap.get(i).getVertex());
			heap.get(i).setValue(temp);
			heap.get(i).setVertex(vertexTmp);
			i = i / 2;
		}	
		size = heap.size();
	}

	public void build(Collection<T> unordered) {

		heap = new ArrayList<Node>();
		root = BinHeap.this.new Node(0);


		if(unordered == null){
			try {
				throw new Exception("Null heap!");
			} catch (Exception e1) {}
		}
		else if(unordered.isEmpty())
			return ;
		else{
			Node node;


			for(Iterator<T> itr = unordered.iterator(); itr.hasNext();){
				node = BinHeap.this.new Node(heap.size());
				T temp = itr.next();
				if(temp!= null)
					node.setValue(temp);
				else{
					try {
						throw new Exception("Null in list");
					} catch (Exception e){
						return;
					}
				}

				heap.add(node);
			}
			size = heap.size();
			int innerNodes = (heap.size()/2) - 1;

			for(int i =  innerNodes; i  >= 0; --i){
				heapify(heap.get(i));
			}
		}

	}
	public void sortHeap(){
		for(int i = size-1; i > 0; --i){
			exchangeInHeap(i);
			heapify(heap.get(0));
		}
		
		size = heap.size();
	}
	public void exchangeInHeap(int i){
		T temp = heap.get(i).getValue();
		heap.get(i).setValue(heap.get(0).getValue()); // swap heap(1) and heap(i)
		heap.get(0).setValue(temp);
		--size;
	}

	public ArrayList<Node> getHeap(){
		return heap;
	}
	
	public void decreaseKey(T key, int v){
		int i;
		for(i = 0; i < heap.size(); i++){
			if(heap.get(i).getVertex() == v)
				break;
		}
		if(key.compareTo(heap.get(i).getValue()) > 0){
			throw new RuntimeException("new key is larger than current key");
		}
		heap.get(i).setValue(key);
		
		while(i > 0 && (heap.get(i/2).getValue().compareTo(heap.get(i).getValue()) > 0)){

			T temp = heap.get(i/2).getValue();
			heap.get(i/2).setValue(heap.get(i).getValue());
			int vertex = heap.get(i/2).getVertex();
			heap.get(i/2).setVertex(heap.get(i).getVertex());
			heap.get(i).setValue(temp);
			heap.get(i).setVertex(vertex);
			i = i / 2;
		}
	}
	
	public boolean contains(int vertex){
		for(int i = 0; i < heap.size(); i++){
			if(heap.get(i).getVertex() == vertex)
				return true;
		}
		return false;
	}
}