package eg.edu.alexu.csd.filestructure.graphs;

import java.io.File;

public class Main {

	public static void main(String[] args) {
		Graph g = new Graph();
		g.readGraph(new File("file.txt"));
		int[] distances = new int[9];
 		g.runDijkstra(0, distances);
 		g.runBellmanFord(0, distances);
	}

}
