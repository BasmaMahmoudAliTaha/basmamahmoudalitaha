package eg.edu.alexu.csd.filestructure.sort;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class BinHeap <T extends Comparable<T>> implements IHeap<T> {
	private ArrayList<INode<T>> heap;
	private INode<T> root;
	private int size;

	public BinHeap() {
		heap = new ArrayList<INode<T>>();
		root = BinHeap.this.new Node(0);
	}

	private class Node implements INode<T>{

		private T value;
		private int index;

		private Node(int index) {
			this.index = index;
		}

		@Override
		public INode<T> getLeftChild() {
			int index = 2 * this.index +1;
			if(index < BinHeap.this.size)
				return BinHeap.this.heap.get(index);
			else 
				return null;
		}

		@Override
		public INode<T> getRightChild() {
			int index = (2 * this.index +1) +1;
			if(index < BinHeap.this.size)
				return BinHeap.this.heap.get(index);
			else 
				return null;

		}

		@Override
		public INode<T> getParent() {
			int index = this.index/2;
			if(index < BinHeap.this.size)
				return BinHeap.this.heap.get(index);
			else 
				return null;

		}

		@Override
		public T getValue() {

			return value;
		}

		@Override
		public void setValue(T value) {

			this.value = value;
		}

	}
	@Override
	public INode<T> getRoot() {

		if(!heap.isEmpty()){
			root = heap.get(0);
			return root;
		}
		else
			return null;
		
	}

	@Override
	public int size() {
		return heap.size();
	}

	@Override
	public void heapify(INode<T> node) {

		try{

			boolean leLarge = false, rLarge = false;
			T left = null;
			if(node.getLeftChild()!=null){ // null means it doesn't have child
				left = node.getLeftChild().getValue();
			}
			T right = null;

			if(node.getRightChild()!=null){
				right = node.getRightChild().getValue();
			}
			T largest;

			if(left != null && left.compareTo(node.getValue()) > 0){
				largest = left;
				leLarge = true;
			}


			else
				largest = node.getValue();

			if(right != null  &&right.compareTo(largest)> 0){
				largest = right;
				rLarge = true;
			}


			if(largest != null &&largest.compareTo(node.getValue()) != 0){


				if(rLarge){
					node.getRightChild().setValue(node.getValue());
					rLarge = false;
					node.setValue(largest);
					heapify(node.getRightChild());
				}
				else if(leLarge){
					node.getLeftChild().setValue(node.getValue());
					leLarge = false;
					node.setValue(largest);
					heapify(node.getLeftChild());
				}

			}
		}catch(Exception e){
			return;
			//			try {
			//				throw new Exception("Null Input");
			//			} catch (Exception e1) {}
		}


	}

	@Override
	public T extract() {
		T max = null;
		try {

			max = this.getRoot().getValue();
			heap.get(0).setValue(heap.get(size()-1).getValue());
			heap.get(size()-1).setValue(max);
			heap.remove(heap.size()-1);

			size = heap.size();

			if(size != 0)
				heapify(heap.get(0));


		} catch (Exception e) {

			try {
				throw new Exception("extracting from empty heap");
			} catch (Exception e1) {}
		}
		return max;

	}

	@Override
	public void insert(T element) {

		if(element == null)
			return;

		if(heap.size() == 0){
			root = BinHeap.this.new Node(0);
			root.setValue(element);
			heap.add(root);
			return;
		}
		INode<T> node = BinHeap.this.new Node(this.size());
		node.setValue(element);
		heap.add(node);
		int i = heap.size()-1;
		while(i > 0 && (heap.get(i/2).getValue().compareTo(heap.get(i).getValue()) < 0)){

			T temp = heap.get(i/2).getValue();
			heap.get(i/2).setValue(heap.get(i).getValue());
			heap.get(i).setValue(temp);
			i = i / 2;
		}	
		size = heap.size();
	}

	@Override
	public void build(Collection<T> unordered) {

		heap = new ArrayList<INode<T>>();
		root = BinHeap.this.new Node(0);


		if(unordered == null){
			try {
				throw new Exception("Null heap!");
			} catch (Exception e1) {}
		}
		else if(unordered.isEmpty())
			return ;
		else{
			INode<T> node;


			for(Iterator<T> itr = unordered.iterator(); itr.hasNext();){
				node = BinHeap.this.new Node(heap.size());
				T temp = itr.next();
				if(temp!= null)
					node.setValue(temp);
				else{
					try {
						throw new Exception("Null in list");
					} catch (Exception e){
						return;
					}
				}

				heap.add(node);
			}
			size = heap.size();
			int innerNodes = (heap.size()/2) - 1;

			for(int i =  innerNodes; i  >= 0; --i){
				heapify(heap.get(i));
			}
		}

	}
	public void sortHeap(){
		for(int i = size-1; i > 0; --i){
			exchangeInHeap(i);
			heapify(heap.get(0));
		}
		
		size = heap.size();
	}
	public void exchangeInHeap(int i){
		T temp = heap.get(i).getValue();
		heap.get(i).setValue(heap.get(0).getValue()); // swap heap(1) and heap(i)
		heap.get(0).setValue(temp);
		--size;
	}

	public ArrayList<INode<T>> getHeap(){
		return heap;
	}
}